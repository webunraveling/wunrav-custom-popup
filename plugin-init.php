<?php
/*
 * Plugin Name: WebUnraveling Custom Popup
 * Plugin URI: https://gitlab.com/webunraveling/wunrav-custom-popup
 * description: A lightweight popup with customizable message and call to action. Once a user has closed the popup or clicked the call to action button, the popup will not appear to them again for 24 hours.
 * Version: 1.0
 * Author: Web Unraveling
 * Author URI: https://webunraveling.com
 * License GPLv3 or later
 */

require_once('custom-popup.php');

$Wunrav_Custom_Popup = new Wunrav_Custom_Popup;
