function closeIt() {
    $('.wunrav-custom-popup').css('opacity', 0);
    $('.wunrav-custom-popup-overlay').css('opacity', 0);
    setTimeout(function() {
        $('.wunrav-custom-popup').css('display', 'none');
        $('.wunrav-custom-popup-overlay').css('display', 'none'); 
    }, 500);

    var date = new Date();
    date.setTime(date.getTime() + (1 * 24 * 60 * 60 * 1000));
    var expires = "expires=" + date.toUTCString();
    document.cookie = "wunravCustomPopup=1;" + expires + ";path=/";
} 

// Open popup
$(document).ready(function() {
    if ($.cookie('wunravCustomPopup')) {
        return;
    } else {
    $('.wunrav-custom-popup').css('opacity', 1);
    $('.wunrav-custom-popup-overlay').css('opacity', 0.8);
    $('.wunrav-custom-popup').css('display', 'block');
    $('.wunrav-custom-popup-overlay').css('display', 'block'); 
    }
});

// close button clicked
$('.wunrav-custom-popup-close').click(function() {
    closeIt();
});

// donate button clicked
$('.wunrav-custom-popup-button').click(function() {
    closeIt();
});
