<?php

/*****************************************
 * This displays an HTML/JS popup which
 * has  a simple message and button.
 *****************************************/

class Wunrav_Custom_Popup {

    protected $slug = 'wunrav-custom-popup';
    protected $name = 'Custom Popup';

    public $options;

    public function __construct() {
        $this->options = get_option($this->slug . '-settings');

        add_action( 'admin_menu', array($this, 'admin_menu_init') );
        add_action( 'admin_init', array($this, 'admin_page_init') );

        // load frontend
        if ( isset($this->options['enable']) ) {
            add_action( 'wp_head', array($this, 'load_style') );
            add_action( 'wp_footer', array($this, 'put_footer') );
        }
    }

    public function admin_menu_init() {
        add_options_page(
            $this->name . ' Options',
            $this->name, 'manage_options',
            $this->slug, array($this, 'callback_admin_page') );
    }

    public function callback_admin_page() {
        if ( ! current_user_can('manage_options') ) {
            wp_die( __('You do not have sufficient permissions to access this page.') );
        }

        wp_enqueue_media();
        wp_enqueue_script( $this->slug . '-media-uploader', plugins_url('/media-uploader.js', __FILE__ ) );
        wp_enqueue_style( $this->slug . 'admin', plugins_url('/custom-popup-admin.css', __FILE__ ) );

        echo '<div class="wrap">';
        echo '<h1>' . $this->name . ' Settings</h1>';
        echo '<p>This is a popup which appears on every page. When one of the buttons in the popup are pressed, the popup will not appear again for 24 hours.</p>';

        echo '<form method="post" action="options.php">';
        settings_fields( $this->slug . '-settings' );
        do_settings_sections( $this->slug );
        submit_button();
        echo '</form></div>';
    }

    public function admin_page_init() {
        register_setting(
            $this->slug . '-settings', // options group
            $this->slug . '-settings', // options name
            array($this, 'sanitize')
        );

        add_settings_section(
            $this->slug . '-main-settings', // section ID
            'Settings for ' . $this->name, // section header
            array($this, 'callback_main_settings'), // callback function
            $this->slug // page
        );

        add_settings_field(
            'enable', // id
            'Enable', // title
            array($this, 'callback_enable'), // callback function
            $this->slug, // page
            $this->slug . '-main-settings' // section
        );

        add_settings_field(
            'image', // id
            'Image', // title
            array($this, 'callback_image'), //callback function
            $this->slug, //page
            $this->slug . '-main-settings' //section
        );

        add_settings_field(
            'message', // id
            'Message', // title
            array($this, 'callback_message'), // callback function
            $this->slug, // page
            $this->slug . '-main-settings' // section
        );

        add_settings_field(
            'button_text', // id
            'Button Text', // title
            array($this, 'callback_button_text'), // callback function
            $this->slug, // page
            $this->slug . '-main-settings' // section
        );

        add_settings_field(
            'button_url', // id
            'Button URL', // title
            array($this, 'callback_button_url'), // callback function
            $this->slug, // page
            $this->slug . '-main-settings' // section
        );
    }

    public function sanitize( $input ) {
        $new_input = array();

        if ( isset($input['enable']) ){
            $new_input['enable'] = filter_var($input['enable'], FILTER_VALIDATE_BOOLEAN);
        }

        if ( isset($input['image']) ) {
            $new_input['image'] = esc_url_raw($input['image']);
        }

        if ( isset($input['message']) ) {
            $new_input['message'] = sanitize_text_field($input['message']);
        }

        if ( isset($input['button_text']) ) {
            $new_input['button_text'] = sanitize_text_field($input['button_text']);
        }

        if ( isset($input['button_url']) ) {
            $new_input['button_url'] = esc_url_raw($input['button_url']);
        }

        return $new_input;
    }

    public function callback_main_settings() {
        /* nothing to do here */
    }

    public function callback_enable() {
        printf(
            '<input type="checkbox" name="' . $this->slug . '-settings[enable]" %s />Check to enable the popup.',
            checked( isset($this->options['enable']), true, false)
        );
    }

    public function callback_image() {
        $img_url = ( $this->options['image'] ?? esc_url_raw($this->options['image']) );

        echo '<div class="wunrav-custom-popup-uploader-target">';
        echo '<input type="text" size="60" name="' . $this->slug .'-settings[image]" value="' . $img_url . '" />';
        echo '<br />';
        echo '<a href="#" class="wunrav-custom-popup-upload-button">Select or upload image</a>';
        echo '<div' . ( $img_url ? '' : ' style="display:none;"' ) . '><br /><img src="' . $img_url . '" style="width:150px;" /></div>';
        echo '</div>';
    }

    public function callback_message() {
        printf(
            '<input type="text" size="60" name="' . $this->slug .'-settings[message]" value="%s" />',
            ( isset( $this->options['message']) ? esc_attr( $this->options['message']) : '' )
        );
    }

    public function callback_button_text() {
        printf(
            '<input type="text" size="60" name="' . $this->slug .'-settings[button_text]" value="%s" />',
            ( isset( $this->options['button_text']) ? esc_attr( $this->options['button_text']) : '' )
        );
    }

    public function callback_button_url() {
        printf(
            '<input type="text" size="60" name="' . $this->slug .'-settings[button_url]" value="%s" />',
            ( isset( $this->options['button_url']) ? esc_url_raw( $this->options['button_url']) : '' )
        );
    }

    public function load_style() {
        wp_enqueue_style( $this->slug, plugins_url('/custom-popup.css', __FILE__) );
    }

    public function put_footer() {
        wp_enqueue_script( $this->slug, plugins_url('/custom-popup.js', __FILE__ ) ); 
        wp_enqueue_script( $this->slug . 'jquery-cookie', plugins_url('/jquery.cookie.js', __FILE__ ) );
        wp_enqueue_script( 'jquery' ); // load from WP core if already enqueued, this is ignored

        $image_url = $this->options['image'] ?? '';
        $message = $this->options['message'] ?? '';
        $button_url = $this->options['button_url'] ?? '';
        $button_text = $this->options['button_text'] ?? 'Click Me';

        echo '<div id="fade" class="wunrav-custom-popup-overlay"></div>';
        echo '<div class="wunrav-custom-popup">';
        echo '<div class="wunrav-custom-popup-img">';
        echo '<img src="' . $image_url . '" />';
        echo '</div>';
        echo '<div class="wunrav-custom-popup-text">';
        echo '<p class="wunrav-custom-popup-message">' . $message . '</p>';
        echo '<a class="wunrav-custom-popup-button" href="' . $button_url . '" rel="noopener nofollow" target="_blank">' . $button_text . '</a>';
        echo '</div>';
        echo '<a class="wunrav-custom-popup-close" href="#">✖</a>';
        echo '</div>';

    }
}
